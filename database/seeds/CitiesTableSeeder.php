<?php

use Illuminate\Database\Seeder;
use LisDev\Delivery\NovaPoshtaApi2;
use App\Models\City;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $np = new NovaPoshtaApi2('b374c51f01e0cb9cb07c9350b8e360b5');
        $result = $np
            ->model('Address')
            ->method('getCities')
            ->execute();
        if(count($result)){
            foreach ( collect($result['data']) as $item) {
                City::firstOrCreate([
                    'description' => $item['Description'],
                    'description_ru' => $item['DescriptionRu'],
                    'ref' => $item['Ref'],
                    'delivery1' => $item['Delivery1'],
                    'delivery2' => $item['Delivery2'],
                    'delivery3' => $item['Delivery3'],
                    'delivery4' => $item['Delivery4'],
                    'delivery5' => $item['Delivery5'],
                    'delivery6' => $item['Delivery6'],
                    'delivery7' => $item['Delivery7'],
                    'area' => $item['Area'],
//                'conglomerates'=>$item['Conglomerates'],
                    'city_id' => $item['CityID'],
                ]);
            }
        }
    }
}
