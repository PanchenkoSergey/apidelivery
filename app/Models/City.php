<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\City
 *
 * @property integer $id
 * @property string $description
 * @property string $description_ru
 * @property string $ref
 * @property string $delivery1
 * @property string $delivery2
 * @property string $delivery3
 * @property string $delivery4
 * @property string $delivery5
 * @property string $delivery6
 * @property string $delivery7
 * @property string $area
 * @property string $conglomerates
 * @property integer $city_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\City whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\City whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\City whereDescriptionRu($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\City whereRef($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\City whereDelivery1($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\City whereDelivery2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\City whereDelivery3($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\City whereDelivery4($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\City whereDelivery5($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\City whereDelivery6($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\City whereDelivery7($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\City whereArea($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\City whereConglomerates($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\City whereCityId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\City whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\City whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class City extends Model
{
    protected $table = 'cities';
    protected $guarded = ['id'];
}
