<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/','IndexController@index')->name('home');
Route::post('/api/get_cities', 'ApiController@getCity')->name('get_cities');
Route::post('/api/get_offices', 'ApiController@getOfficeList')->name('get_offices');
Route::post('/api/calc_delivery', 'ApiController@calcCostDelivery')->name('calc_delivery');
