<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Http\Requests;
use LisDev\Delivery\NovaPoshtaApi2;

class ApiController extends Controller
{
    public function getCity()
    {
        $search_str = mb_convert_case(request()->get('q'), MB_CASE_TITLE, 'UTF-8');
        return json_encode(City::where('description_ru', 'like', $search_str . '%')->limit(50)->get());
    }

    public function getOfficeList($cityName = null)
    {
        $np = new NovaPoshtaApi2('b374c51f01e0cb9cb07c9350b8e360b5');

        $ref = request()->get('city');
        $result = $np
            ->model('AddressGeneral')
            ->method('getWarehouses')
            ->params(array(
                'CityRef' => $ref,
            ))
            ->execute();

        return json_encode($result['data']);
    }

    public function calcCostDelivery()
    {
        $np = new NovaPoshtaApi2('b374c51f01e0cb9cb07c9350b8e360b5');

        $result = $np->model('InternetDocument')
            ->method('getDocumentPrice')
            ->params([
                    'CitySender' => request()->get('CitySender'),
                    'CityRecipient' => request()->get('CityRecipient'),
                    'Weight' => request()->get('Weight'),
                    'ServiceType' => "DoorsDoors",
                    'Cost' => request()->get('Cost'),
                ]
            )
            ->execute();

        return json_encode(collect($result['data'])->first());
    }
}
