<nav @if(!Auth::check())class="light-blue lighten-1" @else class=" orange" @endif

role="navigation">
    <div class="nav-wrapper container">
        <a id="logo-container" href="{{route('home')}}" class="brand-logo">Api Delivery</a>
        <ul class="right hide-on-med-and-down">
            {{--<li>--}}
                {{--<a class="modal-trigger" href="#sign_in">Sign in</a>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<a class="modal-trigger" href="#sign_up">Sign up</a>--}}
            {{--</li>--}}
        </ul>
    </div>
</nav>