<!DOCTYPE html>
<html lang="en">
    @include('layout._components.head')
<body>
    @include('layout._components.head_menu')

    @yield('content')


    @include('layout._components.footer')
    @include('layout._components.modals')
</body>
</html>
