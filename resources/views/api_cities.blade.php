@extends('layout.index')
@section('content')

    <div class="container main">
    <div class="row">
        <div class="row">
            <div class="col s12">
                    <label>Выберете город отправления</label>
                    <select name="departure" class="browser-default" id="city_departure" style="width: 100%">
                        <option ></option>
                    </select>
            </div>
            <br>
            <br>
            <div class="col s12">
                    <label>Выберете город доставки</label>
                    <select name="delivery" class="browser-default" id="city_delivery" style="width: 100%">
                        <option ></option>
                    </select>
            </div>
        </div>
        <span  class="select_office"  style="display: none">Выберите отделение доставки на карте</span>
        <div id="map" style="width: 900px; height: 500px; display: none">

        </div>
    </div>

        <div class="row calc_form" style="display: none">
            <form class="col s12" id="calc_form">
                <div class="row">
                    <div class="input-field col s6">
                        <input style="color: red;" disabled placeholder="Город отправления" id="city_departure_input" type="text" class="validate">
                        <label for="city_departure_input">Город отправления</label>
                    </div>
                    <div class="input-field col s6">
                        <input style="color: red;"  disabled placeholder="Город доставки" id="city_delivery_input" type="text" class="validate">
                        <label for="city_delivery_input">Город доставки</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="weight" type="text" class="validate">
                        <label for="weight">Вес фактический</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="cost" type="text" class="validate">
                        <label for="cost">Объявленная стоимость</label>
                    </div>
                </div>
                <a class="waves-effect waves-light btn calc_delivery">рассчитать</a>
                <div class="row">
                    <div class="input-field col s12 cost_f" style="display: none">
                        <input style="color: red;" disabled placeholder="СТОИМОСТЬ" id="cost_f"  type="text" class="validate">
                        <label for="cost_f">СТОИМОСТЬ</label>
                    </div>
                </div>
            </form>
        </div>
    </div>

@stop