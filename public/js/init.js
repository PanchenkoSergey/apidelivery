(function($){
  $(function(){
    $('.modal-trigger').leanModal({
          // dismissible: true, // Modal can be dismissed by clicking outside of the modal
          // opacity: .5, // Opacity of modal background
          // in_duration: 300, // Transition in duration
          // out_duration: 200, // Transition out duration
          // starting_top: '4%', // Starting top style attribute
          // ending_top: '10%', // Ending top style attribute
        }
    );
    $('.button-collapse').sideNav({
          menuWidth: 300, // Default is 240
          edge: 'right', // Choose the horizontal origin
          closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        }
    );
  $('.carousel').carousel();

  }); // end of document ready
})(jQuery); // end of jQuery name space