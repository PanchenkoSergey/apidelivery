$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function () {
    if ($('#city_departure').length) {
        var city_departure_select = $('#city_departure');
        city_departure_select.select2({
            ajax: {
                url: "/api/get_cities",
                dataType: 'json',
                allowClear: true,
                delay: 250,
                method: 'POST',
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    var data_arr = [];
                    if (data) {
                        $.each(data, function (k, item) {
                            data_arr.push({
                                id: item.ref,
                                text: item.description_ru
                            });
                        })
                    }
                    return {
                        results: data_arr,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            // minimumInputLength: 1,
            allowClear: true,
            placeholder: "Выберете город отправления"
        });
    }

    if ($('#city_delivery').length) {
        var city_delivery_select = $('#city_delivery');
        city_delivery_select.select2({
            ajax: {
                url: "/api/get_cities",
                dataType: 'json',
                allowClear: true,
                delay: 250,
                method: 'POST',
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    var data_arr = [];
                    if (data) {
                        $.each(data, function (k, item) {
                            data_arr.push({
                                id: item.ref,
                                text: item.description_ru
                            });
                        })
                    }
                    return {
                        results: data_arr,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            // minimumInputLength: 1,
            allowClear: true,
            placeholder: "Выберете город доставки"
        });
    }
    city_delivery_select.on('select2:select', function (evt) {
        console.log($(this));
        $('.select_office').show();
        $('#map').show();

        var myLatLng = {lat: 48.643794, lng: 32.268151};

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 6,
            center: myLatLng
        });


        $.ajax({
            url: '/api/get_offices',
            dataType: "json",
            method: 'POST',
            data: {'city': $(this).val()},
            success: function (data, textStatus) {
                $.each(data, function (i, val) {
                    var latitude = parseFloat(val.Latitude);
                    var longitude = parseFloat(val.Longitude);
                    var marker = new google.maps.Marker({
                        position: {lat: latitude, lng: longitude},
                        animation: google.maps.Animation.DROP,
                        map: map,
                        title: val.DescriptionRu
                    });
                    marker.addListener('click', function () {
                        map.setZoom(12);
                        map.setCenter(marker.getPosition());
                        calc_form_show();
                    });
                });
            },
            error: function (data) {
                alert('Попробуйте ещё раз');
            }
        });

    });

    city_delivery_select.on("select2:unselect", function (e) {
        $('.select_office').hide();
        $('#map').hide();
        $('.calc_form').hide();
    });

    function calc_form_show() {
        $('.calc_form').show();
        $('#city_departure_input').val($('#select2-city_departure-container').attr('title'));
        $('#city_delivery_input').val($('#select2-city_delivery-container').attr('title'));
        $('html, body').animate({ scrollTop: $('.calc_form').offset().top }, 500);
    }
    
    $('.calc_delivery').click(function (e) {
        e.preventDefault();
        var data = {
            'CitySender' : city_departure_select.val(),
            'CityRecipient' : city_delivery_select.val(),
            'Weight' : $('#weight').val(),
            'Cost' : $('#cost').val()
        };
        
        $.ajax({
            url: '/api/calc_delivery',
            dataType: "json",
            method: 'POST',
            data: data,
            success: function (data, textStatus) {
                console.dir(data);
                $('.cost_f').show();
                $('#cost_f').val(data.Cost);
            },
            error: function (data) {
                alert('Попробуйте ещё раз');
            }
        });
    });
});
